﻿/*
 * Created by SharpDevelop.
 * User: zach.read
 * Date: 28-08-15
 * Time: 14:13
 * 
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AutoPrinter_SN
{
	/// <summary>
	/// Description of LoginForm.
	/// </summary>
	public partial class LoginForm : Form
	{
		public static int usergroup = 0;
		public LoginForm()
		{
			InitializeComponent();
			
		}
		
		void Textbox1_KeyPress (object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r')
			{
				textBox2.Focus();
				textBox2.Select();
			}		
			
		}
		void Textbox2_KeyPress (object sender, KeyPressEventArgs e)
		{
			if(e.KeyChar == '\r')
			{
				button1.Focus();
				Button1Click(sender,e);
			}
			
			
		}
		void Button1Click(object sender, EventArgs e)
		{
			string connectionString = MainForm.getSQLString();
			using (SqlConnection con = new SqlConnection(connectionString))
			{
				con.Open();
				using (SqlCommand command = new SqlCommand("select UserGroup from userlogin where userNM = '"+textBox1.Text+"' and userPW = '"+textBox2.Text+"' and (UserGroup = 10 or UserGroup = 9)", con))
			    using (SqlDataReader reader = command.ExecuteReader())
				{
		    		if(reader.Read() && reader != null)
		    		{
		    			usergroup = Convert.ToInt16(reader.GetValue(0));
		    			this.Hide();
		    			Form mf = new MainForm(textBox1.Text);
						mf.ShowDialog();
						this.Close();	
						
		    		}
		    		else{
		    			MessageBox.Show("Sorry either this user does not exsit or it doesn't have sufficient privileges.");
		    		}
				}
			}
		}
	}
}
