﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 7/14/2016
 * Time: 4:34 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;

namespace AutoPrinter_SN
{
	/// <summary>
	/// Description of SameSnForm.
	/// </summary>
	public partial class SameSnForm : Form
	{
		private static string AZPL;
        private static string APrinter;  
        private static string APrinterA;        
        private static string APrinterB;
        private static string APrinterC;
        private static string APrinterD;
        private static string AManualA;
        private static string AManualB;
        private static string AManualC;
        private static string AManualD;        
        private static string AUser;
	
		public SameSnForm(string Username)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			AInitializeZPL();
			AUser = Username;
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
		
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
		void Bt_PrintClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(Tb_DSN.Text.ToString()) || Tb_DSN.Text.ToString().Length != 14)
				return;
			
			string SN =Tb_DSN.Text.ToString();
			if(IsFlashTested(SN))
			{
				MessageBox.Show("Sn is Flash Tested");
				return;
			}
			if(IsNotProduced(SN)) {
				MessageBox.Show("Sn has never been produced");
				return;
			}
			
			
			DialogResult dialogResult = MessageBox.Show("do you want to print ?", "Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
			    //do something
			    Label lb = new Label();
				lb.sn = SN;
				lb.Qty = 1;
				AconnectToSql(SN,SN);
			    PrintLabel.print(lb, AZPL , AManualA);
						  	
				Clearall();
			    
			}
			else if (dialogResult == DialogResult.No)
			{
				Clearall();
				return;
			}

			
		}
		
		bool IsFlashTested(string sn)
		{
			string sql = @"select fnumber from ElecParaTest where fnumber ='"+sn+"'";
			SqlDataReader rd = MainForm.GetDataReader(sql);
			if(rd!= null && rd.Read())
			{
				return true;
			}
			return false;
			
		}
		bool IsNotProduced(string sn)
		{
			string sql = @"select ID from SapAutoSerialNumbers where FirstSN <= '" +sn+ "' and LastSN >= '"+sn+"' and FirstSN like '"+sn.Substring(0,10)+"%'";
			SqlDataReader rd = MainForm.GetDataReader(sql);
			if(rd!= null && rd.Read())
			{
				return false;
			}
			return true;
		}
		public static void AInitializeZPL()
        {
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\Config.xml";

            XmlReader reader = XmlReader.Create(xmlFile);
            reader.ReadToFollowing("ZPL");
            AZPL = reader.ReadElementContentAsString();
            reader.Read();
            APrinterA = reader.ReadElementContentAsString();
            reader.Read();
            APrinterB = reader.ReadElementContentAsString();
            reader.Read();
           	APrinterC = reader.ReadElementContentAsString();
            reader.Read();
            APrinterD = reader.ReadElementContentAsString();
            reader.Read();
           	AManualA = reader.ReadElementContentAsString();
           	reader.Read();
           	AManualB = reader.ReadElementContentAsString();            
           	reader.Read();
           	AManualC = reader.ReadElementContentAsString();            
           	reader.Read();
           	AManualD = reader.ReadElementContentAsString();           
           	
            
            reader.Close();
        }
		
		void Clearall()
		{
			Tb_DSN.Text ="";
			Tb_NSN.Text ="";
		}
		
		public static void AconnectToSql(String Sn_F, String Sn_L){
			string connectionString = MainForm.getSQLString();
    		String sql = "INSERT INTO [SapDuplicationSN] VALUES(@firstSN, @SecondSN, @Date, @User)";
			
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn)){
    				cmd.Parameters.AddWithValue("@firstSN", Sn_F);
	                cmd.Parameters.AddWithValue("@SecondSN", Sn_L);
	                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
	                cmd.Parameters.AddWithValue("@User", AUser.ToLower());
	    			cmd.ExecuteNonQuery();
    			}                
    			}                
    		}
				
		
		
	}
}
