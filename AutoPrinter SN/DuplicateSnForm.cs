﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 5/3/2016
 * Time: 1:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;

namespace AutoPrinter_SN
{
	/// <summary>
	/// Description of DuplicateSnForm.
	/// </summary>
	public partial class DuplicateSnForm : Form
	{
		private static string AZPL;
        private static string APrinter;  
        private static string APrinterA;        
        private static string APrinterB;
        private static string APrinterC;
        private static string APrinterD;
        private static string AManualA;
        private static string AManualB;
        private static string AManualC;
        private static string AManualD;        
        private static string AUser;
	
		public DuplicateSnForm(string Username)
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			AInitializeZPL();
			AUser = Username;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void Bt_CancelClick(object sender, EventArgs e)
		{
			this.Close();
		}
		void Bt_PrintClick(object sender, EventArgs e)
		{
			if(string.IsNullOrEmpty(Tb_DSN.Text.ToString()) || Tb_DSN.Text.ToString().Length != 14)
				return;
			int count = -1;
			string SN ="";
			string sql = @"select top 1 newSN from [SapDuplicationSN] where [DuplicatedSN] ='"+Tb_DSN.Text.ToString()+"' order by ID desc";
			SqlDataReader rd = MainForm.GetDataReader(sql);
			if(rd.Read()&& rd!=null)
			{
				MessageBox.Show("This Sn has already been reprinted once before!");
				return;
			}
		
			sql = @"select top 1 newSN from [SapDuplicationSN] where [NewSN] like '"+ Tb_DSN.Text.ToString().Substring(0,6)+"_"+ Tb_DSN.Text.ToString().Substring(7,3) +"%' order by ID desc";
			rd = MainForm.GetDataReader(sql);
			if(rd.Read()&& rd!=null)
			{
				SN = rd.GetString(0);
			}
			if(SN == "")
			{
				SN = Tb_DSN.Text.ToString().Substring(0,6)+ "5" + Tb_DSN.Text.ToString().Substring(7,3) + "0001";
			}
			else
			{
				SN = Tb_DSN.Text.ToString().Substring(0,6)+ "5" + Tb_DSN.Text.ToString().Substring(7,3) + (Convert.ToInt64(SN.Substring(SN.Length-3,3))+1).ToString().PadLeft(4, '0');
			}
			
			Tb_NSN.Text = SN;
			sql = @"select count(fnumber) from ElecParaTest where fnumber = '"+Tb_DSN.Text.ToString()+"'";
			
			rd = MainForm.GetDataReader(sql);
			if(rd.Read()&& rd!=null)
			{
				count = Convert.ToInt16(rd.GetValue(0));
			}
			if(count < 1)
			{
				MessageBox.Show("This SN have not been produced before!");
				return;
			}
			count = -1;
			sql =  @"select count(fnumber) from ElecParaTest_detail where fnumber = '"+Tb_DSN.Text.ToString()+"'";
			rd = MainForm.GetDataReader(sql);
			if(rd.Read()&& rd!=null)
			{
				count = Convert.ToInt16(rd.GetValue(0));
			}
			count = count + 1;
			DialogResult dialogResult = MessageBox.Show("SN "+ Tb_DSN.Text.ToString() +" has been tested "+ count.ToString() +" times, do you want to print ?", "Attention", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
			    //do something
			    Label lb = new Label();
				lb.sn = SN;
				lb.Qty = 1;
				AconnectToSql(Tb_DSN.Text.ToString(),SN);
			    PrintLabel.print(lb, AZPL , AManualA);
						  	
				Clearall();
			    
			}
			else if (dialogResult == DialogResult.No)
			{
				Clearall();
				return;
			}

			
		}
		
		public static void AInitializeZPL()
        {
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\Config.xml";

            XmlReader reader = XmlReader.Create(xmlFile);
            reader.ReadToFollowing("ZPL");
            AZPL = reader.ReadElementContentAsString();
            reader.Read();
            APrinterA = reader.ReadElementContentAsString();
            reader.Read();
            APrinterB = reader.ReadElementContentAsString();
            reader.Read();
           	APrinterC = reader.ReadElementContentAsString();
            reader.Read();
            APrinterD = reader.ReadElementContentAsString();
            reader.Read();
           	AManualA = reader.ReadElementContentAsString();
           	reader.Read();
           	AManualB = reader.ReadElementContentAsString();            
           	reader.Read();
           	AManualC = reader.ReadElementContentAsString();            
           	reader.Read();
           	AManualD = reader.ReadElementContentAsString();           
           	
            
            reader.Close();
        }
		
		void Clearall()
		{
			Tb_DSN.Text ="";
			Tb_NSN.Text ="";
		}
		
		public static void AconnectToSql(String Sn_F, String Sn_L){
			string connectionString = MainForm.getSQLString();
    		String sql = "INSERT INTO [SapDuplicationSN] VALUES(@firstSN, @SecondSN, @Date, @User)";
			
    		using (SqlConnection conn = new SqlConnection(connectionString))
        	{
    			conn.Open();
    			using (SqlCommand cmd = new SqlCommand(sql, conn)){
    				cmd.Parameters.AddWithValue("@firstSN", Sn_F);
	                cmd.Parameters.AddWithValue("@SecondSN", Sn_L);
	                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
	                cmd.Parameters.AddWithValue("@User", AUser.ToLower());
	    			cmd.ExecuteNonQuery();
    			}                
    			}                
    		}
				
	
	}
}
