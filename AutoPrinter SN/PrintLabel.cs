﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoPrinter_SN
{
    class PrintLabel
    {
        public PrintLabel()
        {
        }

        //print label using ZPL template
        public static bool print(Label lb, string ZPL, string printer)
        {
            ZPL = ZPL.Replace("A1111111111111", lb.sn);		
			ZPL = ZPL.Replace("^PQ3", "^PQ" + lb.Qty); 			

            return RawPrinterHelper.SendStringToPrinter(printer, ZPL, "");
        }
    }
}
