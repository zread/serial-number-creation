﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 5/3/2016
 * Time: 1:39 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace AutoPrinter_SN
{
	partial class DuplicateSnForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox Tb_DSN;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button Bt_Print;
		private System.Windows.Forms.Button Bt_Cancel;
		private System.Windows.Forms.TextBox Tb_NSN;
		private System.Windows.Forms.Label label2;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.Tb_DSN = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.Bt_Print = new System.Windows.Forms.Button();
			this.Bt_Cancel = new System.Windows.Forms.Button();
			this.Tb_NSN = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// Tb_DSN
			// 
			this.Tb_DSN.Location = new System.Drawing.Point(117, 35);
			this.Tb_DSN.Name = "Tb_DSN";
			this.Tb_DSN.Size = new System.Drawing.Size(128, 20);
			this.Tb_DSN.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(12, 38);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 19);
			this.label1.TabIndex = 1;
			this.label1.Text = "Duplicated SN:";
			// 
			// Bt_Print
			// 
			this.Bt_Print.Location = new System.Drawing.Point(46, 90);
			this.Bt_Print.Name = "Bt_Print";
			this.Bt_Print.Size = new System.Drawing.Size(75, 27);
			this.Bt_Print.TabIndex = 2;
			this.Bt_Print.Text = "Print";
			this.Bt_Print.UseVisualStyleBackColor = true;
			this.Bt_Print.Click += new System.EventHandler(this.Bt_PrintClick);
			// 
			// Bt_Cancel
			// 
			this.Bt_Cancel.Location = new System.Drawing.Point(162, 90);
			this.Bt_Cancel.Name = "Bt_Cancel";
			this.Bt_Cancel.Size = new System.Drawing.Size(83, 25);
			this.Bt_Cancel.TabIndex = 3;
			this.Bt_Cancel.Text = "Cancel";
			this.Bt_Cancel.UseVisualStyleBackColor = true;
			this.Bt_Cancel.Click += new System.EventHandler(this.Bt_CancelClick);
			// 
			// Tb_NSN
			// 
			this.Tb_NSN.Enabled = false;
			this.Tb_NSN.Location = new System.Drawing.Point(117, 61);
			this.Tb_NSN.Name = "Tb_NSN";
			this.Tb_NSN.Size = new System.Drawing.Size(128, 20);
			this.Tb_NSN.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(12, 64);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 19);
			this.label2.TabIndex = 5;
			this.label2.Text = "New SN:";
			// 
			// DuplicateSnForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(274, 155);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.Tb_NSN);
			this.Controls.Add(this.Bt_Cancel);
			this.Controls.Add(this.Bt_Print);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.Tb_DSN);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "DuplicateSnForm";
			this.Text = "DuplicateSnForm";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
