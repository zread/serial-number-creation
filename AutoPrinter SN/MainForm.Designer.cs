﻿/*
 * Created by SharpDevelop.
 * User: Zach.Read
 * Date: 27-08-15
 * Time: 10:29
 * 
 */
namespace AutoPrinter_SN
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.CbPrint = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.Bt_Duplicate = new System.Windows.Forms.Button();
			this.BtSame = new System.Windows.Forms.Button();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(49, 63);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(163, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Please Select a Line:";
			// 
			// comboBox1
			// 
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Items.AddRange(new object[] {
			"A",
			"B",
			"C"});
			this.comboBox1.Location = new System.Drawing.Point(218, 58);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(49, 28);
			this.comboBox1.TabIndex = 1;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(84, 11);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(402, 33);
			this.label4.TabIndex = 6;
			this.label4.Text = "Printing AutoPrinter Serial Numbers";
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.Location = new System.Drawing.Point(218, 189);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(126, 30);
			this.button1.TabIndex = 4;
			this.button1.Text = "Print Labels";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(218, 103);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(73, 20);
			this.textBox1.TabIndex = 7;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(49, 100);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(163, 23);
			this.label2.TabIndex = 8;
			this.label2.Text = "Production Order:";
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(49, 141);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(163, 23);
			this.label3.TabIndex = 10;
			this.label3.Text = "Print Qty:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(218, 144);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(73, 20);
			this.textBox2.TabIndex = 9;
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 222);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(564, 22);
			this.statusStrip1.TabIndex = 11;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(86, 17);
			this.toolStripStatusLabel1.Text = "Version Sap 1.0";
			// 
			// CbPrint
			// 
			this.CbPrint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CbPrint.FormattingEnabled = true;
			this.CbPrint.Items.AddRange(new object[] {
			"Auto",
			"Manual"});
			this.CbPrint.Location = new System.Drawing.Point(403, 58);
			this.CbPrint.Name = "CbPrint";
			this.CbPrint.Size = new System.Drawing.Size(83, 28);
			this.CbPrint.TabIndex = 12;
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(301, 61);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 31);
			this.label5.TabIndex = 13;
			this.label5.Text = "Print Mode:";
			// 
			// Bt_Duplicate
			// 
			this.Bt_Duplicate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Bt_Duplicate.Location = new System.Drawing.Point(380, 189);
			this.Bt_Duplicate.Name = "Bt_Duplicate";
			this.Bt_Duplicate.Size = new System.Drawing.Size(126, 30);
			this.Bt_Duplicate.TabIndex = 14;
			this.Bt_Duplicate.Text = "Duplication";
			this.Bt_Duplicate.UseVisualStyleBackColor = true;
			this.Bt_Duplicate.Click += new System.EventHandler(this.Bt_DuplicateClick);
			// 
			// BtSame
			// 
			this.BtSame.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BtSame.Location = new System.Drawing.Point(380, 141);
			this.BtSame.Name = "BtSame";
			this.BtSame.Size = new System.Drawing.Size(126, 30);
			this.BtSame.TabIndex = 15;
			this.BtSame.Text = "SAME SN";
			this.BtSame.UseVisualStyleBackColor = true;
			this.BtSame.Click += new System.EventHandler(this.BtSameClick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(564, 244);
			this.Controls.Add(this.BtSame);
			this.Controls.Add(this.Bt_Duplicate);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.CbPrint);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.label1);
			this.Name = "MainForm";
			this.Text = "AutoPrinter SN";
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox CbPrint;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button Bt_Duplicate;
		private System.Windows.Forms.Button BtSame;
		
		
	}
}
