Program Created: 
Initially created for use with K3, and then adopted to work with SAP.

Created By;
Zach Read

Program Used By: 
Production

Program's Purpose: 
Used to print serial numbers to a variety of printers. Checks for duplicates and will only print on the correct line with the correct workshop numbers. Used with auto printers and manual printer in supervisors office.

Deployed:
Supervisors office computer

Referneces:
None.


